/*
 * TextQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef TEXTQUESTION_H_
#define TEXTQUESTION_H_

#include "Question.h"

class textQuestion: public EnqueteQuestion {
public:
	textQuestion(const unsigned int& vectorSize); // Constructor

	void parseQuestion(const string& vraag);
};

#endif /* TEXTQUESTION_H_ */
