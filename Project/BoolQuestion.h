/*
 * BoolQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef BOOLQUESTION_H_
#define BOOLQUESTION_H_

#include "Question.h"

class boolQuestion: public EnqueteQuestion {
public:
	boolQuestion(const unsigned int& vectorSize): EnqueteQuestion(vectorSize) {}
    
    void parseQuestion(const string& vraag) {
        setQuestionType("BOOL");
        setQuestionText(vraag);
    }
};

#endif /* BOOLQUESTION_H_ */
