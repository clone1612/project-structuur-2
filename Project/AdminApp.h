//
//  AdminApp.h
//  Project
//
//  Created by Jannick Hemelhof on 13/01/14.
//  Copyright (c) 2014 Jannick Hemelhof. All rights reserved.
//

#ifndef __Project__AdminApp__
#define __Project__AdminApp__

#include <iostream>
#include "Enquete.h"
#include <Wt/WApplication>
#include <Wt/WBootstrapTheme>
#include <Wt/WCheckBox>
#include <Wt/WContainerWidget>
#include <Wt/WCssDecorationStyle>
#include <Wt/WLength>
#include <Wt/WLineEdit>
#include <Wt/WMessageBox>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTable>
#include <Wt/WFileUpload>
#include <Wt/WProgressBar>
#include <Wt/WDialog>

using namespace Wt;

class AdminApp : public WApplication {
public:
	AdminApp(const WEnvironment& env, vector<Enquete>& enqVector);
    
	void addRow(vector<Enquete>& enqVector, WTable* table, const int idx);
    void deleteEnquete(vector<Enquete>& enqVector, const int idx);
	void dumpJSON(vector<Enquete>& enqVector, const int idx);
private:
	WTable * table;
	WLineEdit * enqTitle;
};

#endif /* defined(__Project__AdminApp__) */