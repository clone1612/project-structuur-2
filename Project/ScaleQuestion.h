/*
 * ScaleQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef SCALEQUESTION_H_
#define SCALEQUESTION_H_

#include "Question.h"

class scaleQuestion: public EnqueteQuestion {
public:
	scaleQuestion(const unsigned int vectorSize): EnqueteQuestion(vectorSize), scaleLow_(0), scaleHigh_(0) {}
    
    void parseQuestion(const string &vraag, const int &scaleLow, const int &scaleHigh) {
        setQuestionType("SCALE");
        setQuestionText(vraag);
        scaleLow_ = scaleLow;
        scaleHigh_ = scaleHigh;
    }

	const unsigned int& scaleLow() const {
        return scaleLow_;
    }
    
    const unsigned int& scaleHigh() const {
        return scaleHigh_;
    }
	
    void update_scaleLow(const unsigned int& new_scaleLow) {
        scaleLow_ = new_scaleLow;
    }
    
    void update_scaleHigh(const unsigned int& new_scaleHigh) {
        scaleHigh_ = new_scaleHigh;
    }
private:
	unsigned int scaleLow_, scaleHigh_;
};

#endif /* SCALEQUESTION_H_ */
