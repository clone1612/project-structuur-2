//
//  UserApp.h
//  Project
//
//  Created by Jannick Hemelhof on 13/01/14.
//  Copyright (c) 2014 Jannick Hemelhof. All rights reserved.
//

#ifndef __Project__UserApp__
#define __Project__UserApp__

#include <iostream>
#include "Enquete.h"
#include <Wt/WApplication>
#include <Wt/WBootstrapTheme>
#include <Wt/WCheckBox>
#include <Wt/WContainerWidget>
#include <Wt/WCssDecorationStyle>
#include <Wt/WLength>
#include <Wt/WLineEdit>
#include <Wt/WMessageBox>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTable>
#include <Wt/WFileUpload>
#include <Wt/WProgressBar>
#include <Wt/WDialog>

using namespace Wt;

class UserApp : public WApplication {
public:
	UserApp(const WEnvironment& env, vector<Enquete>& enqVector);
    void startEnquete(const int& idx, vector<Enquete>& enqVector);
private:
	WTable * table;
	WLineEdit * enqueteEntry;
};

#endif /* defined(__Project__UserApp__) */
