/*
 * questionPath.h
 *
 *  Created on: 28 nov. 2013
 *      Author: Jannick
 */

#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

#ifndef questionPath_H_
#define questionPath_H_

class questionPath {
public:
	questionPath(const int &forLoop = 0): idx_(1) { // forLoop = 1 -> path zal worden gebruikt om door de enquete te wandelen
        number_.resize(1);
        if (forLoop == 1) {
            number_[0] = 1;
            number_.push_back(1);
        }
    }

	void parseString(const string &str1);
	void hadGroup(const int backwards = 0);
	void hadSentinel(const int backwards = 0);
	void hadOther(const int backwards = 0);
    bool operator==(const questionPath& path2) const;
    bool operator<(const questionPath &path2) const;
    bool operator>(const questionPath &path2) const;
    bool sameLevel(questionPath& path2) const;
    bool before(questionPath& path2) const;
    const int lengthPath(questionPath& path2) const;
	void printPath(const int noWhitespace = 0) const;
    void printPath(ofstream& output, const int noWhitespace = 0) const;
	
    const int& number_at_idx(const int& idx) const {
        return number_[idx];
    }
private:
    int idx_;
	vector<int> number_;
};

#endif /* questionPath_H_ */
