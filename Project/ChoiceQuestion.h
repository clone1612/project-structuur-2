/*
 * ChoiceQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef CHOICEQUESTION_H_
#define CHOICEQUESTION_H_

#include "Question.h"
#include <vector>

class choiceQuestion: public EnqueteQuestion {
public:
	choiceQuestion(const unsigned int& vectorSize): amountChoices_(0), EnqueteQuestion(vectorSize) {
        choices_.resize(1);
    }
    
    void parseQuestion(const string& vraag, vector<string> choices, const int& amountChoices) {
        setQuestionType("CHOICE");
        setQuestionText(vraag);
        choices_ = choices;
        amountChoices_ = amountChoices;
    }
    
    const int& amountChoices() {
        return amountChoices_;
    }
    
    void printChoices() const {
        int idx = 1, num = 1;
        while (idx <= amountChoices_) {
            cout << num << ") " << choices_[idx] << endl;
            idx++;
            num++;
        }
    }
    
    const string& choiceAt(const int &idx) const {
        return choices_[idx];
    }
    
    void setAmountChoices(const unsigned int& newAmountChoices) {
        amountChoices_ = newAmountChoices;
    }
    
    void setChoices(const vector<string> newChoices) {
        choices_ = newChoices;
    }

private:
    int amountChoices_;
	vector<string> choices_;
};

#endif /* CHOICEQUESTION_H_ */
