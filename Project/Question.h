/*
 * Question.h
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include <iostream>
#include <vector>
using namespace std;

#ifndef QUESTION_H_
#define QUESTION_H_

class EnqueteQuestion {
public:
    EnqueteQuestion(const unsigned int& vectorSize):
    vectorSize(vectorSize), typeIndex_(0), textIndex_(1), answerIndex_(2), answered_(0), optional_(false), next_(NULL), prev_(NULL) {
        thequestion_.resize(vectorSize);
    }
    
    // Print de vraag
    virtual void show_question() const {
        cout << thequestion_.at(typeIndex_) << " " << thequestion_.at(textIndex_) << endl;
    }
    
    const string& questionType() const {
        return thequestion_.at(typeIndex_);
    }
    
    const string& questionText() const {
        return thequestion_.at(textIndex_);
    }
    
    const string& questionAnswer() const {
        return thequestion_.at(answerIndex_);
    }
    
    // Past het vraagtype van de vraag aan
    void setQuestionType(const string& newQuestionType) {
        thequestion_[typeIndex_] = newQuestionType;
    }
    
    // Past de vraagtekst van de vraag aan
    void setQuestionText(const string& newQuestionText) {
        thequestion_[textIndex_] = newQuestionText;
    }
    
    // Past het antwoord van de vraag aan
    void setQuestionAnswer(const string& answer) {
        thequestion_[answerIndex_] = answer;
        answered_ = 1;
    }
    
    // Past de status van de vraag aan (beantwoord = 1, niet beantwoord = 0)
    void setAnswerend(const int &newStatus) {
        answered_ = newStatus;
    }
    
    // Geeft terug of de vraag al is beantwoord
    bool is_answered() {
        if (answered_ == 1) {
            return true;
        }
        return false;
    }
    
    // Is de vraag misschien optioneel?
    bool optional() {
        return optional_;
    }
    void setOptional(const bool& optional) {
        optional_ = optional;
    }

	int vectorSize, typeIndex_, textIndex_, answerIndex_, answered_;
    bool optional_;
    vector<string> thequestion_;
    EnqueteQuestion* next_;
    EnqueteQuestion* prev_;
};

#endif /* QUESTION_H_ */
