/*
 * questionPath.cpp
 *
 *  Created on: 28 nov. 2013
 *      Author: Jannick
 */

#include "questionPath.h"
#include <sstream>
#include <algorithm>
using namespace std;

bool IsOdd (int i) {
	return ((i%2)==1);
}

void questionPath::parseString(const string &str1) {
    istringstream ss(str1);
    string temp;
    int count = 1;
    while (getline(ss, temp, '.')) {
        istringstream ssTemp(temp);
        int num;
        ssTemp >> num;
        number_.push_back(num);
        count++;
    }
    number_[0] = count - 1;
}

void questionPath::hadGroup(const int backwards) {
    if (backwards == 0) {
        idx_++;
        number_[0] = number_[0] + 1;
        number_[idx_] = 1;
    }
    else {
        number_[0]--;
        idx_--;
    }
}

void questionPath::hadSentinel(const int backwards) {
    if (backwards == 0) {
        number_[0] = number_[0] - 1;
        idx_ = idx_ - 1;
        number_[idx_] = number_[idx_] + 1;
    }
    else {
        number_[0]++;
        number_[idx_]--;
        idx_++;
    }
}

void questionPath::hadOther(const int backwards) {
    if (backwards == 0) {
        number_[idx_] = number_[idx_] + 1;
    }
    else {
        number_[idx_]--;
    }
}

void questionPath::printPath(const int noWhitespace) const {
	unsigned int count = 1, max = number_[0];
    if (noWhitespace == 0) {
        if (max != 1) {
            for (int tmp = 1; tmp < max; tmp++) {
                cout << "   ";
            }
        }
    }
	while (count <= max) {
		cout << number_[count] << ".";
		count++;
	}
}

void questionPath::printPath(ofstream &output, const int noWhitespace) const {
    unsigned int count = 1, max = number_[0];
    if (noWhitespace == 0) {
        if (max != 1) {
            for (int tmp = 1; tmp < max; tmp++) {
                output << "   ";
            }
        }
    }
	while (count <= max) {
		output << number_[count] << ".";
		count++;
	}
}

bool questionPath::sameLevel(questionPath &path2) const {
    int pathLevel = path2.number_at_idx(0);
    if (number_[0] == pathLevel) {
        return true;
    }
    return false;
}

bool questionPath::before(questionPath &path2) const {
    int pathLevel = path2.number_at_idx(0), curLevel = number_[0];
    if (number_[curLevel] < path2.number_at_idx(pathLevel)) {
        return true;
    }
    return false;
}

const int questionPath::lengthPath(questionPath &path2) const {
    int pathLevel = path2.number_at_idx(0), result;
    result = path2.number_at_idx(pathLevel) - number_[pathLevel] + 1;
    return result;
}

bool questionPath::operator==(const questionPath& path2) const{
	unsigned int count = 0, max = number_[0] + 1;
	while (count < max) {
		if (path2.number_at_idx(count) != number_[count]) {
			return false;
		}
		count++;
	}
	return true;
}

bool questionPath::operator<(const questionPath &path2) const {
    int levelPath1 = number_[0], levelPath2 = path2.number_at_idx(0);
    if (levelPath1 == levelPath2) {
        if (number_[levelPath1] < path2.number_at_idx(levelPath2)) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (levelPath1 < levelPath2) {
        if (number_[levelPath1] < path2.number_at_idx(levelPath1)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (number_[levelPath2] < path2.number_at_idx(levelPath2)) {
            return true;
        }
        else {
            return false;
        }
    }
}

bool questionPath::operator>(const questionPath &path2) const {
    int levelPath1 = number_[0], levelPath2 = path2.number_at_idx(0);
    if (levelPath1 == levelPath2) {
        if (number_[levelPath1] > path2.number_at_idx(levelPath2)) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (levelPath1 < levelPath2) {
        if (number_[levelPath1] > path2.number_at_idx(levelPath1)) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        if (number_[levelPath2] > path2.number_at_idx(levelPath2)) {
            return true;
        }
        else {
            return false;
        }
    }
}