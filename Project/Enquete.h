/*
 * Enquete.h
 *
 *  Created on: 25 nov. 2013
 *      Author: Jannick
 */

#include <iostream>
#include <vector>
#include <stack>
#include "Question.h"
#include "GroupQuestion.h"
#include "questionPath.h"
#include <Wt/WString>
using namespace std;

#ifndef ENQUETE_H_
#define ENQUETE_H_

class Enquete {
public:
	Enquete(): root_(new EnqueteQuestion(5)), last_(root_), alreadyAnswered_(false), groupID_(1), timesAnswered_(0) {
        answersVector_.resize(1);
    }
	// Print alle vragen in onze lijst
	void list();
    void listAnswered();
    bool everythingAnswered();

	// Controleer of het type vraag geldig is
	static bool checkQuestionType(const string& q_type);
    bool checkPath(const questionPath& path);
    bool checkGroupPath(const questionPath &path);
    bool checkPathAsker(const questionPath &path);
    void updatePath(EnqueteQuestion* curQuestion, questionPath &curPath, const int backwards = 0);
    void updatePath(stack<groupQuestion*> &groupStack, EnqueteQuestion* curQuestion, questionPath &curPath);

	// Controleer of twee strings gelijk zijn (niet-case-sensitive)
	static bool checkCaseInsensitive(const string& a, const string& b); // 2 strings controleer -> gelijk case-insensitive

	void setID(const string& newID) {
        id_ = newID;
    }

	void setVersion(const unsigned int& newVersion) {
        version_ = newVersion;
    }

	// Voeg een nieuwe vraag toe aan onze lijst
	void add(const string& soort, const string& vraag, const bool& optional, const unsigned int& scaleLow = 0, const unsigned int& scaleHigh = 0);
	void add(const string& vraag, vector<string> choices, const int& n_choices, const bool& optional);
    void insert(const string& soort, const string& vraag, const questionPath& path, const int& scaleLow = 0, const int& scaleHigh = 0, const int& beforeSentinel = 0);

	void add_group(const string& name, const int& amountSubgroups, const bool& optional); // Voeg groep toe op einde v/d enquete
    void prepareGroupVector(vector<int>& groupVector);
    void useGroupVector(vector<int>& groupVector);
	void add_group(const string& name, questionPath& path1, questionPath& path2, const int& amountSubgroups);
	void ungroup(questionPath& path);

	const int amountQuestions(); // Geeft terug hoeveel vragen er in een lijst zijn

	void edit(const questionPath& path); // Pas de vraagtekst van een specifieke vraag aan
    void setChoices(const questionPath& path); // Pas de keuzes voor een specifieke choice-vraag aan

	void remove(const questionPath& path); // Verwijdert een specifieke vraag

	void setChanged(const int& changedStatus) {
        if (changedStatus == 0) {
            changed_ = 0;
        }
        else if (changedStatus == 1) {
            changed_ = 1;
        }
    }

	bool notSaved() const {
        return changed_ == 1;
    }

	void save(const string& filename); // Slaat de lijst op
    void saveAnswers(const string &filename);
    
    typedef const EnqueteQuestion& const_reference;
    typedef const EnqueteQuestion* const_pointer;
    typedef EnqueteQuestion* pointer;
    
    class Iterator: public iterator<bidirectional_iterator_tag, EnqueteQuestion> {
    public:
        Iterator(): questionPointer_(NULL) {}
        Iterator(EnqueteQuestion* q) : questionPointer_(q) {}
        ~Iterator() {}
        
        Iterator& operator=(const Iterator& other) {
            questionPointer_ = other.questionPointer_;
            return *this;
        }
        
        bool operator==(const Iterator& other) {
            return (questionPointer_ == other.questionPointer_);
        }
        
        bool operator!=(const Iterator& other) {
            return (questionPointer_ != other.questionPointer_);
        }
        
        Iterator& operator++() {
            if (questionPointer_ != NULL) {
                questionPointer_ = questionPointer_->next_;
            }
            return *this;
        }
        
        Iterator& operator--() {
            if (questionPointer_ != NULL) {
                questionPointer_ = questionPointer_->prev_;
            }
            return *this;
        }
        
        Iterator operator++(int) {
            Iterator tmp(*this);
            ++(*this);
            return tmp;
        }
        
        Iterator operator--(int) {
            Iterator tmp(*this);
            --(*this);
            return tmp;
        }
        
        EnqueteQuestion* operator*()
        {
            return questionPointer_;
        }
        
        pointer& operator->()
        {
            return(questionPointer_);
        }
        
    private:
        EnqueteQuestion* questionPointer_;
    };
    
    Iterator insert_after(EnqueteQuestion* qBefore, EnqueteQuestion* qNew);
    Iterator insert_before(EnqueteQuestion* qAfter, EnqueteQuestion* qNew);
    
    void push_back(EnqueteQuestion *qNew) {
        insert_after(last_, qNew);
    }
    
    void pop_back() {
        last_ = last_->prev_;
        delete last_;
    }
    
    bool checkGroupAnswered(Iterator it);
    Iterator erase(EnqueteQuestion* q);
    Iterator begin() {
        return Iterator(root_->next_);
    }
    Iterator end() {
        return Iterator();
    }
    const Wt::WString& name() const {
        return name_;
    }
    void setName(const Wt::WString& newName) {
        name_ = newName;
    }
    const int& timesAnswered() const {
        return timesAnswered_;
    }
    void hasAnswered() {
        timesAnswered_++;
        alreadyAnswered_ = true;
    }
    const bool& alreadyAnswered() {
        return alreadyAnswered_;
    }
    void pushAnswers(const vector<string>& answers) {
        answersVector_.push_back(answers);
    }
    void saveAnswers(const vector<string>& answers, const string& filename);
    const vector<vector<string>>& answersVector() {
        return answersVector_;
    }
private:
	EnqueteQuestion* root_;
    EnqueteQuestion* last_;
	string id_;
    Wt::WString name_;
    vector<vector<string>> answersVector_;
    bool alreadyAnswered_;
	int version_ = 1, changed_ = 0, groupID_, timesAnswered_;
};

#endif /* ENQUETE_H_ */
