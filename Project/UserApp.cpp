//
//  UserApp.cpp
//  Project
//
//  Created by Jannick Hemelhof on 13/01/14.
//  Copyright (c) 2014 Jannick Hemelhof. All rights reserved.
//

#include "UserApp.h"
#include "Enquete.h"
#include "ChoiceQuestion.h"
#include "ScaleQuestion.h"
#include "GroupQuestion.h"
#include <algorithm>
#include <list>
#include <set>

#include <boost/regex.hpp>
#include <Wt/WApplication>
#include <Wt/WBootstrapTheme>
#include <Wt/WCheckBox>
#include <Wt/WContainerWidget>
#include <Wt/WCssDecorationStyle>
#include <Wt/WLength>
#include <Wt/WLineEdit>
#include <Wt/WMessageBox>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTable>
#include <Wt/WLabel>
#include <Wt/WLineEdit>
#include <Wt/WFileUpload>
#include <Wt/WProgressBar>
#include <Wt/WDialog>
#include <Wt/WValidator>
#include <Wt/WComboBox>
#include <Wt/WContainerWidget>
#include <Wt/WSelectionBox>
#include <Wt/WSlider>
#include <Wt/WFormWidget>
#include <Wt/WLink>

#include <uuid/uuid.h>
#include <vector>
#include <iostream>

UserApp::UserApp(const WEnvironment& env, vector<Enquete>& enqVector): WApplication(env) {
    setTitle("Beschikbare Enquetes");
    setTheme(new WBootstrapTheme());
    
    root()->setContentAlignment(Wt::AlignTop | Wt::AlignLeft);
    
    root()->addWidget(new WText("<h1>Beschikbare Enquetes</h1>"));
    
    table = new WTable();
    table->setWidth(WLength(50, WLength::Percentage));
    table->setHeaderCount(1);
    table->addStyleClass("table form-inline");
    table->toggleStyleClass("table-hover", true);
    table->toggleStyleClass("table-bordered", true);
    table->elementAt(0, 0)->addWidget(new WText("Naam enquete"));
    table->elementAt(0, 1)->addWidget(new WText("Enquete invullen"));
    root()->addWidget(table);
    
    int numberElements = enqVector.size() - 1;
    if (numberElements != 0) {
        for (int idx = 1; idx <= numberElements; ++idx ) {
            if (!enqVector[idx].alreadyAnswered()) { // Mag nog niet ingevuld zijn!
                const WString name = enqVector[idx].name();
                const int row = table->rowCount();
                
                Wt::WPushButton * button = new WPushButton("Start de enquete");
                button->clicked().connect(boost::bind(&UserApp::startEnquete, this, idx, ref(enqVector)));
                table->elementAt(row, 1)->addWidget(button);
                table->elementAt(row, 0)->addWidget(new WText(name));
            }
        }
    }
    
    root()->addWidget(new WBreak());
}

void UserApp::startEnquete(const int& idx, vector<Enquete>& enqVector) {
    root()->clear();
    Enquete & enq = enqVector[idx];
    setTitle(enq.name());
    root()->addWidget(new WText("<h1>" + enq.name() + "</h1>"));
    stack<groupQuestion*> groupStack;
    bool optionalGroup = false;
    for (Enquete::Iterator it = enq.begin(); it != enq.end(); ++it) {
        if (it->questionType() == "GROUP") {
            // Controleer of group optioneel is
            groupQuestion * gq = dynamic_cast<groupQuestion *>(*it);
            groupStack.push(gq);
            if (gq->optional_) {
                optionalGroup = true;
            }
            continue;
        }
        if (it->questionType() == "SENTINEL") {
            // Pop een group v/d stack. Stack = empty? Dan optionalGroup = false
            groupStack.pop();
            if (groupStack.empty()) {
                optionalGroup = false;
            }
            continue;
        }
        string questionText = it->questionText();
        Wt::WValidator * validator = new Wt::WValidator(false);
        if (!it->optional_ && !optionalGroup) {
            questionText = questionText + "* ";
            validator->setMandatory(true);
        }
        Wt::WLabel * label = new Wt::WLabel(questionText);
        root()->addWidget(label);
        if (it->questionType() == "TEXT") {
            Wt::WLineEdit * answer = new Wt::WLineEdit();
            root()->addWidget(answer);
            answer->setValidator(validator);
            root()->addWidget(new Wt::WBreak());
        }
        else if (it->questionType() == "BOOL") {
            Wt::WComboBox * box = new Wt::WComboBox(root());
            box->addItem("<Maak een keuze>");
            box->addItem("Ja");
            box->addItem("Neen");
            box->setValidator(validator);
            root()->addWidget(new Wt::WBreak());
        }
        else if (it->questionType() == "CHOICE") {
            Wt::WComboBox * box = new Wt::WComboBox(root());
            box->addItem("<Maak een keuze>");
            choiceQuestion * cq = dynamic_cast<choiceQuestion *>(*it);
            for (int idx = 1; idx <= cq->amountChoices(); ++idx) {
                string choice = cq->choiceAt(idx);
                box->addItem(choice);
            }
            box->setValidator(validator);
            root()->addWidget(new Wt::WBreak());
        }
        else if (it->questionType() == "SCALE") {
            Wt::WSlider * slider = new Wt::WSlider(root());
            slider->resize(250, 25);
            slider->setTickPosition(Wt::WSlider::TicksAbove);
            slider->setTickInterval(1);
            scaleQuestion * sq = dynamic_cast<scaleQuestion *>(*it);
            slider->setMinimum(sq->scaleLow() - 1); // -1 om optionele scale-vragen te ondersteunen
            slider->setMaximum(sq->scaleHigh());
            slider->setValue(slider->minimum());
            slider->setValidator(validator);
            root()->addWidget(new Wt::WBreak());
        }
    }
    Wt::WPushButton * button = new WPushButton("Opslaan");
    button->clicked().connect(std::bind([&] () {
        bool validAnswers = true;
        vector<string> tempAnswers;
        tempAnswers.resize(1);
        tempAnswers[0] = enq.name().toUTF8();
        int testCount = root()->count();
        for (int idx = 1; idx < root()->count(); ++idx) {
            Wt::WLineEdit * text = dynamic_cast<Wt::WLineEdit *>(root()->widget(idx));
            Wt::WComboBox * box = dynamic_cast<Wt::WComboBox *>(root()->widget(idx));
            Wt::WSlider * slider = dynamic_cast<Wt::WSlider *>(root()->widget(idx));
            if (text != NULL) {
                if (text->validate() == Wt::WValidator::Valid && !text->text().empty()) {
                    // Valid answer dus we mogen doorgaan en antwoord opslaan
                    tempAnswers.push_back(text->text().toUTF8());
                    continue;
                }
                else if (!text->validator()->isMandatory()) {
                    // Veld is niet verplicht, we mogen doorgaan
                    tempAnswers.push_back("NULL");
                    continue;
                }
                else {
                    // Ongeldig answer, opslaan wordt geannulleerd
                    validAnswers = false;
                    break;
                }
            }
            else if (box != NULL) {
                if (box->currentIndex() != 0) {
                    // Gebruiker heeft iets geldig geselecteerd, doorgaan en opslaan
                    tempAnswers.push_back(to_string(box->currentIndex()));
                    continue;
                }
                else if (!box->validator()->isMandatory()) {
                    // Veld was niet verplicht, we mogen toch nog doorgaan
                    tempAnswers.push_back("NULL");
                    continue;
                }
                else {
                    // Gebruiker heeft niets geselecteerd terwijl het wel moest
                    validAnswers = false;
                    break;
                }
            }
            else if (slider != NULL) {
                if (slider->value() != slider->minimum()) {
                    // Gebruiker heeft iets geldig geselecteerd, doorgaan en opslaan
                    tempAnswers.push_back(to_string(slider->value()));
                    continue;
                }
                else if (!slider->validator()->isMandatory()) {
                    // Veld was niet verplicht, we mogen toch nog doorgaan
                    tempAnswers.push_back("NULL");
                    continue;
                }
                else {
                    // Gebruiker heeft niets geselecteerd terwijl het wel moest
                    validAnswers = false;
                    break;
                }
            }
        }
        if (validAnswers) {
            // Antwoorden toevoegen aan enquete
            enq.pushAnswers(tempAnswers);
            // Antwoorden opslaan op de harde schijf
            string filename = docRoot() + "/" + enq.name().toUTF8() + "_answers.txt";
            enq.saveAnswers(tempAnswers, filename);
            // functie in enq die vector neemt en antwoorden wegschrijft
            enq.hasAnswered();
            WDialog * dialog_ = new WDialog("Antwoorden opgeslagen");
            new Wt::WText("Uw antwoorden zijn opgeslagen. Druk op OK om naar het keuzemenu te gaan.", dialog_->contents());
            new Wt::WBreak(dialog_->contents());
            Wt::WPushButton * ok = new Wt::WPushButton("OK", dialog_->footer());
            ok->setLink(Wt::WLink(Wt::WLink::InternalPath, ""));
            ok->clicked().connect(std::bind([&] () {
                dialog_->accept();
            }));
            dialog_->show();
        }
     }));
    root()->addWidget(button);
}