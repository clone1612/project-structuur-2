/*
 * GroupQuestion.h
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#ifndef GROUPQUESTION_H_
#define GROUPQUESTION_H_

#include "Question.h"
#include "GroupSentinel.h"

class groupQuestion: public EnqueteQuestion {
public:
	groupQuestion(const unsigned int& vectorSize, const unsigned int& groupID):
    EnqueteQuestion(vectorSize), groupID_(groupID), sentinel_(NULL) {}
    
    void parseQuestion(const string& theme) {
        setQuestionType("GROUP");
        setQuestionText(theme);
    }
    
    const int& groupID() const {
        return groupID_;
    }
    
    const int& amountSubgroups() const {
        return amountSubgroups_;
    }
    
    void setAmountSubgroups(const int &newAmount) {
        amountSubgroups_ = newAmount;
    }
    
    groupSentinel* sentinel() {
        return sentinel_;
    }
    
    void setSentinel(groupSentinel *newSentinel) {
        sentinel_ = newSentinel;
    }
private:
	int groupID_, amountSubgroups_;
    groupSentinel * sentinel_;
};

#endif /* GROUPQUESTION_H_ */
