//
//  AdminApp.cpp
//  Project
//
//  Created by Jannick Hemelhof on 13/01/14.
//  Copyright (c) 2014 Jannick Hemelhof. All rights reserved.
//

#include "AdminApp.h"
#include "Enquete.h"
#include <algorithm>
#include <list>
#include <set>

#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <Wt/WApplication>
#include <Wt/WBootstrapTheme>
#include <Wt/WCheckBox>
#include <Wt/WContainerWidget>
#include <Wt/WCssDecorationStyle>
#include <Wt/WLength>
#include <Wt/WLineEdit>
#include <Wt/WMessageBox>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTable>
#include <Wt/WFileUpload>
#include <Wt/WProgressBar>
#include <Wt/WDialog>
#include <uuid/uuid.h>
#include <vector>
#include <iostream>
#include <string>

#include <Wt/Json/Array>
#include <Wt/Json/Object>
#include <Wt/Json/Serializer>
#include <Wt/Json/Value>

using namespace Wt;
using namespace std;

string get_uuid() {
    uuid_t t;
    char ch[36];
    uuid_generate(t);
    uuid_unparse(t, ch);
    stringstream ss;
    string uuid;
    ss << ch;
    ss >> uuid;
    return uuid;
}

Enquete editor(const string& filename) {
	Enquete enquete;
	ifstream file(filename.c_str());
    vector<int> groupVector;
    groupVector.resize(1);
    groupVector[0] = 0;
    int hadGroups = 0;
	if (file.is_open()) {
		// Bestand bestaat al, we moeten het dus inlezen en verwerken
		int headercount = 1, headerlength = 3, h_version_l = 1, headeridline = 2, headerquestionsline = 3;
		string a_string;
		while (getline(file, a_string)) {
			istringstream ssquestion(a_string);
            
			// We verwerken hier de header van de enquete
			if (headercount <= headerlength) {
				// Versie van de enquete inlezen
				if (headercount == h_version_l) {
					string version_text;
					int version_num;
					ssquestion >> version_text >> version_num;
                    if (version_num > 3) {
                        throw 20;
                    }
                    else {
                        enquete.setVersion(3);
                    }
				}
				// ID van enquete inlezen
				if (headercount == headeridline) {
					string id_text, id_code;
					ssquestion >> id_text >> id_code;
					enquete.setID(id_code);
				}
				// Aantal vragen van de enquete inlezen
				if (headercount == headerquestionsline) {
					string steps_text;
					int steps_num;
					ssquestion >> steps_text >> steps_num;
				}
				headercount++;
				continue;
			}
            
			// We lopen door de eerste twee "woorden" in de stringstream en slaan deze op in hun variabelen
			string questionNum, questionType;
			ssquestion >> questionNum >> questionType;
            
			if (enquete.checkCaseInsensitive(questionType, "choice")) {
				unsigned int num_of_choices;
				ssquestion >> num_of_choices;
				string questionText;
                vector<string> choices;
                choices.resize(1);
                ssquestion >> ws;
                getline(ssquestion, questionText);
                if (questionText.empty()) {
                    throw 20;
                }
                // Keuzes verwerken
                unsigned int c_a_idx = 1, choicecount = 1;
                while (choicecount <= num_of_choices) {
                    string choice;
                    (getline(file, choice));
                    choices.push_back(choice);
                    choicecount++;
                    c_a_idx++;
                }
                bool optional = false;
                int found = questionText.find("#opt");
                if (found != string::npos) {
                    optional = true;
                    questionText = questionText.substr(0, found);
                }
                enquete.add(questionText, choices, num_of_choices, optional);
                groupVector.push_back(0);
                continue;
			}
            if ((enquete.checkCaseInsensitive(questionType, "text")) || (enquete.checkCaseInsensitive(questionType, "bool"))) {
				string questionText;
                ssquestion >> ws;
				getline(ssquestion, questionText); // 'Echte' vraag vastpakken
                if (questionText.empty()) {
                    throw 20;
                }
                bool optional = false;
                int found = questionText.find("#opt");
                if (found != string::npos) {
                    optional = true;
                    questionText = questionText.substr(0, found);
                }
				enquete.add(questionType, questionText, optional);
                groupVector.push_back(0);
                continue;
			}
            if (enquete.checkCaseInsensitive(questionType, "scale")) {
                int l, h;
                string questionText;
                ssquestion >> l >> h;
                ssquestion >> ws;
                getline(ssquestion, questionText);
                if (questionText.empty()) {
                    throw 20;
                }
                bool optional = false;
                int found = questionText.find("#opt");
                if (found != string::npos) {
                    optional = true;
                    questionText = questionText.substr(0, found);
                }
                enquete.add(questionType, questionText, optional, l, h);
                groupVector.push_back(0);
                continue;
            }
            if (enquete.checkCaseInsensitive(questionType, "group")) {
                hadGroups = 1;
                int amountSubgroups;
                string groupTheme;
                ssquestion >> amountSubgroups;
                ssquestion >> ws;
                getline(ssquestion, groupTheme);
                if (groupTheme.empty()) {
                    throw 20;
                }
                bool optional = false;
                int found = groupTheme.find("#opt");
                if (found != string::npos) {
                    optional = true;
                    groupTheme = groupTheme.substr(0, found);
                }
                enquete.add_group(groupTheme, amountSubgroups, optional);
                groupVector.push_back(amountSubgroups);
                groupVector[0]++;
                continue;
            }
		}
        if (hadGroups == 1) {
            enquete.prepareGroupVector(groupVector);
            enquete.useGroupVector(groupVector);
        }
	}
	else {
		// Bestand bestaat nog niet, UUID aanmaken en wegschrijven
        string uuid = get_uuid();
		enquete.setID(uuid);
		enquete.setVersion(2);
	}
    cout << "In file" << endl;
	file.close();
    enquete.setChanged(0);
    return enquete;
}

AdminApp::AdminApp(const WEnvironment& env, vector<Enquete>& enqVector): WApplication(env) {
	setTitle("Enquetes beheren");
	setTheme(new WBootstrapTheme());
    
	root()->setContentAlignment(Wt::AlignTop | Wt::AlignLeft);
    
	root()->addWidget(new WText("<h1>Enquetes beheren</h1>"));
    
	table = new WTable();
	table->setWidth(WLength(50, WLength::Percentage));
	table->setHeaderCount(1);
    table->addStyleClass("table form-inline");
    table->toggleStyleClass("table-hover", true);
    table->toggleStyleClass("table-bordered", true);
	table->elementAt(0, 0)->addWidget(new WText("Naam enquete"));
    table->elementAt(0, 1)->addWidget(new WText("Aantal deelnemers"));
    table->elementAt(0, 2)->addWidget(new WText("Verwijderen"));
    table->elementAt(0, 3)->addWidget(new WText("Antwoorden verzamelen"));
    root()->addWidget(table);
    
	enqTitle = new WLineEdit();
	enqTitle->setWidth(WLength(15, WLength::Percentage));
	enqTitle->setPlaceholderText("Titel enquete");
	enqTitle->setFocus();
	root()->addWidget(enqTitle);
    
	root()->addWidget(new WBreak());
    
    // Lopende enquetes tonen
    int numberElements = enqVector.size() - 1;
    if (numberElements != 0) {
        for (int idx = 1; idx <= numberElements; ++idx ) {
            AdminApp::addRow(enqVector, table, idx);
        }
    }
    
    WFileUpload * fu = new WFileUpload;
    fu->setFileTextSize(50); // Set the maximum file size to 50 kB.
    fu->setProgressBar(new WProgressBar());
    fu->setMargin(10, Wt::Right);
    fu->uploaded().connect(std::bind([=, &enqVector] () {
        const string path = fu->spoolFileName();
        const WString name = enqTitle->text();
        Enquete myEnq = editor(path);
        myEnq.setName(name);
        myEnq.save(docRoot() + "/" + name.toUTF8() + ".txt"); // Enquete opslaan op schijf
        enqVector.push_back(myEnq);
        AdminApp::addRow(enqVector, table, table->rowCount());
    }));
    root()->addWidget(fu);
    
    WPushButton * uploadButton = new WPushButton("Upload");
    uploadButton->setMargin(10, Left | Right);
    uploadButton->clicked().connect(std::bind([=] () {
        fu->upload();
        uploadButton->disable();
    }));
    root()->addWidget(uploadButton);
}

void AdminApp::addRow(vector<Enquete>& enqVector, WTable* table, const int idx) {
    const WString name = enqVector[idx].name();
    const int row = table->rowCount();
    
    WText * enqName = new WText(name);
    WPushButton * deleteButton = new WPushButton("DELETE");
    deleteButton->clicked().connect(std::bind([=, &enqVector] () {
        AdminApp::deleteEnquete(enqVector, idx);
    }));
    WPushButton * jsonButton = new WPushButton("JSON");
    jsonButton->clicked().connect(std::bind([=, &enqVector] () {
        AdminApp::dumpJSON(enqVector, idx);
    }));
    table->elementAt(row, 3)->addWidget(jsonButton);
    table->elementAt(row, 2)->addWidget(deleteButton);
    table->elementAt(row, 1)->addWidget(new WText(std::to_string(enqVector[idx].timesAnswered())));
    table->elementAt(row, 0)->addWidget(enqName);
}

void AdminApp::deleteEnquete(vector<Enquete>& enqVector, const int idx) {
    // Bevestiging vragen
    WDialog * dialog_ = new WDialog("Enquete verwijderen");
    new Wt::WText("Wilt u de enquete echt verwijderen?", dialog_->contents());
    new Wt::WBreak(dialog_->contents());
    Wt::WPushButton * yesButton = new Wt::WPushButton("JA", dialog_->footer());
    Wt::WPushButton * noButton = new Wt::WPushButton("ANNULEER", dialog_->footer());
    yesButton->clicked().connect(std::bind([=, &enqVector] () {
        enqVector.erase(enqVector.begin()+idx);
        this->table->deleteRow(idx);
        boost::filesystem::wpath file(docRoot() + "/" + enqVector[idx].name().toUTF8() + ".txt");
        boost::filesystem::wpath enqAnswers(docRoot() + "/" + enqVector[idx].name().toUTF8() + "_answers.txt");
        boost::filesystem::remove(file); // Verwijder enquete van harde schijf
        boost::filesystem::remove(enqAnswers); // Verwijder antwoorden van harde schijf
        dialog_->accept();
    }));
    noButton->clicked().connect(std::bind([=] () {
        dialog_->reject();
    }));
    dialog_->show();
}

void AdminApp::dumpJSON(vector<Enquete>& enqVector, const int idx) {
    Wt::Json::Value root(Wt::Json::ArrayType);
    Wt::Json::Array& array = root;
    Enquete & enq = enqVector[idx];
    int numberAnswers = enq.answersVector().size() - 1;
	for (int idx1 = 1; idx1 <= numberAnswers; ++idx1) {
        array.push_back(Json::Value(Json::ArrayType));
        Json::Array & arr = array.back();
        int numberQuestions = enq.answersVector()[idx1].size();
        for (int idx2 = 1; idx2 < numberQuestions; ++idx2) {
            string answer = enq.answersVector()[idx1][idx2];
            if (answer == "NULL") {
                Wt::Json::Value json; //NULL value
                arr.push_back(json);
                continue;
            }
            else {
                Wt::Json::Value json(answer);
                arr.push_back(json);
            }
        }
	}
    WMessageBox * box = new WMessageBox("Dump of JSON data", Json::serialize(array), Information, Ok);
	box->textWidget()->setWordWrap(true);
	box->textWidget()->setTextFormat(PlainText);
	box->buttonClicked().connect(box, &Wt::WDialog::accept);
	box->show();
}