/*
 * TextQuestion.cpp
 *
 *  Created on: 1 dec. 2013
 *      Author: Jannick
 */

#include "TextQuestion.h"
#include "Question.h"

textQuestion::textQuestion(const unsigned int& vectorSize):
EnqueteQuestion(vectorSize) {}

void textQuestion::parseQuestion(const string& vraag) {
	setQuestionType("TEXT");
	setQuestionText(vraag);
}
