/*
 * main.cpp
 *
 *  Created on: 28 nov. 2013
 *      Author: Jannick
 */

#include "Enquete.h"
#include "questionPath.h"
#include "ScaleQuestion.h"
#include "ChoiceQuestion.h"
#include "GroupQuestion.h"
#include "GroupSentinel.h"
#include <vector>
#include <sstream>
#include <fstream>
#include <Wt/WApplication>
#include <Wt/WEnvironment>
#include <Wt/WBootstrapTheme>
#include <Wt/WCheckBox>
#include <Wt/WContainerWidget>
#include <Wt/WCssDecorationStyle>
#include <Wt/WLength>
#include <Wt/WLineEdit>
#include <Wt/WMessageBox>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WTable>
#include <Wt/WFileUpload>
#include <Wt/WProgressBar>
#include <Wt/WDialog>
#include <Wt/WServer>
#include <Wt/WFormWidget>
#include "AdminApp.h"
#include "UserApp.h"
using namespace Wt;

Wt::WApplication *createApplication(const Wt::WEnvironment& env, vector<Enquete>& enqVector) {
    
    string path = env.deploymentPath();
    bool admin(path == "/admin");
    if (admin) {
        return new AdminApp(env, enqVector);
    }
    else {
        return new UserApp(env, enqVector);
    }
}

int main(int argc, char **argv) {
    char *custom_argv[] = {
        "hello",
        "--docroot=/usr/local/share/Wt",
        "--http-address=127.0.0.1",
        "--http-port=8080"
    };
    vector<Enquete> enq;
    enq.resize(1);
    try {
        Wt::WServer server(custom_argv[0]);
        
        server.setServerConfiguration(4, custom_argv, WTHTTP_CONFIGURATION);
        
        server.addEntryPoint( Wt::Application,
                             boost::bind(&createApplication, _1, ref(enq)),
                             "/admin");
        
        server.addEntryPoint( Wt::Application,
                             boost::bind(&createApplication, _1, ref(enq)),
                             "");
        
        if (server.start()) {
            Wt::WServer::waitForShutdown();
            server.stop();
        }
        
    } catch (Wt::WServer::Exception& e) {
        std::cerr << e.what() << std::endl;
    } catch (std::exception &e) {
        std::cerr << "exception: " << e.what() << std::endl;
    }
}

