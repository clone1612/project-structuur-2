/*
 * GroupSentinel.h
 *
 *  Created on: 6 dec. 2013
 *      Author: Jannick
 */

#ifndef GROUPSENTINEL_H_
#define GROUPSENTINEL_H_

#include "Question.h"

class groupSentinel: public EnqueteQuestion {
public:
	groupSentinel(const int& groupID): EnqueteQuestion(5), groupID_(groupID) {
        setQuestionType("SENTINEL");
    }
    
    const int& groupID() const {
        return groupID_;
    }
private:
	int groupID_;
};

#endif /* GROUPSENTINEL_H_ */
